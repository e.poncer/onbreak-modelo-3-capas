﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio
{
    public class ActividadEmpresa
    {
        public int IdActividadEmpresa { get; set; }
        public string Descripcion { get; set; }
        public ICollection<Cliente> Cliente { get; set; }

        public ActividadEmpresa()
        {
            this.Init();
        }

        private void Init()
        {
            IdActividadEmpresa = 0;
            Descripcion = string.Empty;
        }

        public bool Read()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.ActividadEmpresa actividad = bbdd.ActividadEmpresa.First(ac => ac.IdActividadEmpresa == IdActividadEmpresa);
                CommonBC.Syncronize(actividad, this);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<ActividadEmpresa> ReadAll()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.ActividadEmpresa> listadoDatosActividad = bbdd.ActividadEmpresa.ToList<Datos.ActividadEmpresa>();

                List<ActividadEmpresa> listadoNegocioActividad = GenerarListadoActividad(listadoDatosActividad);

                return listadoNegocioActividad;
            }
            catch (Exception ex)
            {
                return new List<ActividadEmpresa>();
            }
        }


        private List<ActividadEmpresa> GenerarListadoActividad(List<Datos.ActividadEmpresa> listadoDatosActividad)
        {
            List<ActividadEmpresa> listadoActividad = new List<ActividadEmpresa>();

            foreach(Datos.ActividadEmpresa dato in listadoDatosActividad)
            {
                ActividadEmpresa actividad = new ActividadEmpresa();
                CommonBC.Syncronize(dato, actividad);

                listadoActividad.Add(actividad);
            }
            return listadoActividad;
        }
    }
}
