﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio
{
    public class Valorizador
    {
        public ICollection<Cliente> Cliente { get; set; }

        public Valorizador()
        {

        }

        public double CalcularValorEvento(double valorBase , int cantAsistentes , int cantPersonalAdicional )
        {
            double uf = 27622.65;
            double recargoAsistente = 0;
            double recargoPersonalAdicional = 0;
            double total;

            if (valorBase == 0)
            {
                throw new Exception("El valor base no puede ser cero");
            }else if (valorBase <= 0)
            {
                throw new ArgumentOutOfRangeException("El valor base tiene que ser un numero positivo");
            }

            if (cantAsistentes > 0 & cantAsistentes < 20)
            {
                recargoAsistente = cantAsistentes * (uf * 3);
            }
            if (cantAsistentes >= 21 & cantAsistentes <= 50)
            {
                recargoAsistente = cantAsistentes * (uf * 5);
            }
            if (cantAsistentes > 50)
            {
                recargoAsistente = (uf * 2) * ((cantAsistentes - 50) / 20);
            }

            double contadorCantPersona = 0.5;
            if (cantPersonalAdicional == 2)
            {
                recargoPersonalAdicional = (uf * 2);
            }
            if (cantPersonalAdicional == 3)
            {
                recargoPersonalAdicional = (uf * 3);
            }
            if (cantPersonalAdicional == 4)
            {
                recargoPersonalAdicional = (uf * 3.5);
            }
            if (cantPersonalAdicional > 4)
            {
                recargoPersonalAdicional = (3.5 * uf + contadorCantPersona * (cantPersonalAdicional - 4) * uf);
            }

            total = valorBase + recargoAsistente + recargoPersonalAdicional;
            return total;
        }
    }
}
