﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio
{
    public class TipoEmpresa
    {
        public int IdTipoEmpresa { get; set; }
        public string Descripcion { get; set; }
        public ICollection<Cliente> Cliente { get; set; }

        public TipoEmpresa()
        {
            this.Init();
        }
        
        private void Init()
        {
            this.IdTipoEmpresa = 0;
            this.Descripcion = string.Empty;
        }

        public bool Read()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.TipoEmpresa tipo = bbdd.TipoEmpresa.First(tp => tp.IdTipoEmpresa == IdTipoEmpresa);
                CommonBC.Syncronize(tipo, this);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<TipoEmpresa> ReadAll()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.TipoEmpresa> listadoDatos = bbdd.TipoEmpresa.ToList<Datos.TipoEmpresa>();

                List<TipoEmpresa> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<TipoEmpresa>();
            }
        }

        private List<TipoEmpresa> GenerarListado(List<Datos.TipoEmpresa> listadoTipoEmpresa)
        {
            List<TipoEmpresa> listadoTipo = new List<TipoEmpresa>();

            foreach(Datos.TipoEmpresa dato in listadoTipoEmpresa)
            {
                TipoEmpresa tipo = new TipoEmpresa();
                CommonBC.Syncronize(dato, tipo);

                listadoTipo.Add(tipo);
            }
            return listadoTipo;
        }
    }
}
