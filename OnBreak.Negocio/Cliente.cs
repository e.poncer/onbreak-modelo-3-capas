﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio
{
    public class Cliente
    {
        string _descripcionTipoEmpresa;
        string _descripcionActividadEmpresa;

        public string RutCliente { get; set; }
        public string RazonSocial { get; set; }
        public string NombreContacto { get; set; }
        public string MailContacto { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int IdActividadEmpresa { get; set; }
        public int IdTipoEmpresa { get; set; }
        public string DescripcionActividadEmpresa { get { return _descripcionActividadEmpresa; } }
        public string DescripcionTipoEmpresa { get { return _descripcionTipoEmpresa; } }

        public Cliente()
        {
            this.Init();
        }

        private void Init()
        {
            this.RutCliente = string.Empty;
            this.RazonSocial = string.Empty;
            this.NombreContacto = string.Empty;
            this.MailContacto = string.Empty;
            this.Direccion = string.Empty;
            this.Telefono = string.Empty;
            this.IdActividadEmpresa = 0;
            this.IdTipoEmpresa = 0;
            _descripcionActividadEmpresa = string.Empty;
            _descripcionTipoEmpresa = string.Empty;
        }

        public bool Create()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            Datos.Cliente cli = new Datos.Cliente();

            try
            {
                CommonBC.Syncronize(this, cli);

                bbdd.Cliente.Add(cli);
                bbdd.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Read()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.Cliente cli = bbdd.Cliente.FirstOrDefault(c => c.RutCliente == RutCliente);

                CommonBC.Syncronize(cli,this);

                LeerDescripcionActividadEmpresa();
                LeerDescripcionTipoEmpresa();

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.Cliente cli = bbdd.Cliente.FirstOrDefault(c => c.RutCliente == RutCliente);

                CommonBC.Syncronize(this, cli);

                bbdd.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.Cliente cli = bbdd.Cliente.FirstOrDefault(c => c.RutCliente == RutCliente);

                bbdd.Cliente.Remove(cli);

                bbdd.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void LeerDescripcionActividadEmpresa()
        {
            ActividadEmpresa actividad = new ActividadEmpresa() { IdActividadEmpresa = IdActividadEmpresa };

            if (actividad.Read())
            {
                _descripcionActividadEmpresa = actividad.Descripcion;
            }
            else
            {
                _descripcionActividadEmpresa = string.Empty;
            }
        }

        public void LeerDescripcionTipoEmpresa()
        {
            TipoEmpresa tipo = new TipoEmpresa() { IdTipoEmpresa = IdTipoEmpresa };

            if (tipo.Read())
            {
                _descripcionTipoEmpresa = tipo.Descripcion;
            }
            else
            {
                _descripcionTipoEmpresa = string.Empty;
            }
        }


        public List<Cliente> ReadAll()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Cliente> listadoDatos = bbdd.Cliente.ToList<Datos.Cliente>();

                List<Cliente> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<Cliente>();
            }
        }

        private List<Cliente> GenerarListado(List<Datos.Cliente> listadoDatos)
        {
            List<Cliente> listadoCliente = new List<Cliente>();

            foreach(Datos.Cliente dato in listadoDatos)
            {
                Cliente negocio = new Cliente();
                CommonBC.Syncronize(dato, negocio);
                negocio.LeerDescripcionActividadEmpresa();
                negocio.LeerDescripcionTipoEmpresa();

                listadoCliente.Add(negocio);
            }

            return listadoCliente;
        }

        public List<Cliente> ReadAllByRut(string rut)
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Cliente> listadoDatos = bbdd.Cliente.Where(c => c.RutCliente == rut).ToList<Datos.Cliente>();

                List<Cliente> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<Cliente>();
            }
        }

        public List<Cliente> ReadAllByTipoEmpresa(string tipo)
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Cliente> listadoDatos = bbdd.Cliente.Where(c => c.TipoEmpresa.Descripcion == tipo).ToList<Datos.Cliente>();

                List<Cliente> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<Cliente>();
            }
        }

        public List<Cliente> ReadAllByActividad(int actividad)
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Cliente> listadoDatos = bbdd.Cliente.Where(c => c.ActividadEmpresa.IdActividadEmpresa == actividad ).ToList<Datos.Cliente>();

                List<Cliente> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex) 
            {
                return new List<Cliente>();
            }
        }
    }
}
