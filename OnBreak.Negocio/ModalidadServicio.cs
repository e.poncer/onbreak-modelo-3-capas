﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio
{
    public class ModalidadServicio
    {
        public string IdModalidad { get; set; }
        public int IdTipoEvento { get; set; }
        public string Nombre { get; set; }
        public double ValorBase { get; set; }
        public int PersonalBase { get; set; }
        public ICollection<Contrato> Contrato { get; set; }

        public ModalidadServicio()
        {
            this.Init();
        }

        private void Init()
        {
            this.IdModalidad = string.Empty;
            this.IdTipoEvento = 0;
            this.Nombre = string.Empty;
            this.ValorBase = 0;
            this.PersonalBase = 0;
        }

        public bool Read()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.ModalidadServicio modalidad = bbdd.ModalidadServicio.First(ms => ms.IdModalidad == IdModalidad);
                CommonBC.Syncronize(modalidad, this);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<ModalidadServicio> ReadAll()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.ModalidadServicio> listadoDatos = bbdd.ModalidadServicio.ToList<Datos.ModalidadServicio>();

                List<ModalidadServicio> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<ModalidadServicio>();
            }
        }

        private List<ModalidadServicio> GenerarListado(List<Datos.ModalidadServicio> listadoModalidadServicio)
        {
            List<ModalidadServicio> listadoModalidad = new List<ModalidadServicio>();

            foreach(Datos.ModalidadServicio dato in listadoModalidadServicio)
            {
                ModalidadServicio modalidad = new ModalidadServicio();
                CommonBC.Syncronize(dato, modalidad);

                listadoModalidad.Add(modalidad);
            }
            return listadoModalidad;
        }
    }
}
