﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio
{
    public class Contrato
    {
        string _nombreModalidad;
        string _descripcionEvento;

        public string Numero { get; set; }
        public DateTime Creacion { get; set; }
        public DateTime Termino { get; set; }
        public string RutCliente { get; set; }
        public string IdModalidad { get; set; }
        public int IdTipoEvento { get; set; }
        public string NombreModalidad { get { return _nombreModalidad; } }
        public string DescripcionEvento { get { return _descripcionEvento; } }
        public DateTime FechaHoraInicio { get; set; }
        public DateTime FechaHoraTermino { get; set; }
        public int Asistentes { get; set; }
        public int PersonalAdicional { get; set; }
        public bool Realizado { get; set; }
        public double ValorTotalContrato { get; set; }
        public string Observaciones { get; set; }
        
        public Contrato()
        {
            this.Init();
        }

        private void Init ()
        {
            this.Numero = string.Empty;
            this.Creacion = DateTime.Today;
            this.Termino = DateTime.Today;
            this.RutCliente = string.Empty;
            this.IdModalidad = string.Empty;
            this.IdTipoEvento = 0;
            this.FechaHoraInicio = DateTime.Today;
            this.FechaHoraTermino = DateTime.Today;
            this.Asistentes = 0;
            this.PersonalAdicional = 0;
            this.Realizado = false;
            this.ValorTotalContrato = 0;
            this.Observaciones = string.Empty;
        }

        public bool Create()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            Datos.Contrato cont = new Datos.Contrato();

            try
            {
                CommonBC.Syncronize(this, cont);

                bbdd.Contrato.Add(cont);
                bbdd.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Read()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.Contrato cont = bbdd.Contrato.First(c => c.Numero == Numero);

                CommonBC.Syncronize(cont, this);

                LeerDescripcionEvento();
                LeerNombreModalidad();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.Contrato cont = bbdd.Contrato.First(c => c.Numero == Numero);

                CommonBC.Syncronize(this, cont);

                bbdd.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.Contrato cont = bbdd.Contrato.First(c => c.Numero == Numero);

                bbdd.Contrato.Remove(cont);

                bbdd.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void LeerNombreModalidad()
        {
            ModalidadServicio modalidad = new ModalidadServicio() { IdModalidad = IdModalidad};

            if (modalidad.Read())
            {
                _nombreModalidad = modalidad.Nombre;
            }
            else
            {
                _nombreModalidad = string.Empty;
            }
        }

        public void LeerDescripcionEvento()
        {
            TipoEvento tipo = new TipoEvento() { IdTipoEvento = IdTipoEvento };

            if (tipo.Read())
            {
                _descripcionEvento = tipo.Descripcion;
            }
            else
            {
                _descripcionEvento = string.Empty;
            }
        }


        public List<Contrato> ReadAll()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Contrato> listadoDatos = bbdd.Contrato.ToList<Datos.Contrato>();

                List<Contrato> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<Contrato>();
            }
        }

        private List<Contrato> GenerarListado(List<Datos.Contrato> listadoDatos)
        {
            List<Contrato> listadoContrato = new List<Contrato>();

            foreach(Datos.Contrato dato in listadoDatos)
            {
                Contrato negocio = new Contrato();
                CommonBC.Syncronize(dato, negocio);
                negocio.LeerNombreModalidad();
                negocio.LeerDescripcionEvento();

                listadoContrato.Add(negocio);
            }

            return listadoContrato;
        }

        public List<Contrato> ReadAllByNumeroContrato(string numeroContrato)
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Contrato> listadoDatos = bbdd.Contrato.Where(c => c.Numero == numeroContrato).ToList<Datos.Contrato>();

                List<Contrato> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<Contrato>();
            }
        }

        public List<Contrato> ReadAllByRut(string rut)
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Contrato> listadoDatos = bbdd.Contrato.Where(c => c.RutCliente == rut).ToList<Datos.Contrato>();

                List<Contrato> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<Contrato>();
            }
        }

        public List<Contrato> ReadAllByTipo(string modalidad)
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.Contrato> listadoDatos = bbdd.Contrato.Where(c => c.ModalidadServicio.IdModalidad == modalidad ).ToList<Datos.Contrato>();

                List<Contrato> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<Contrato>();
            }
        }
    }
}
