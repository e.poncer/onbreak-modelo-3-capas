﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio
{
    public class TipoEvento
    {
        public int IdTipoEvento { get; set; }
        public string Descripcion { get; set; }
        public ICollection<ModalidadServicio> ModalidadServicio { get; set; }

        public TipoEvento()
        {
            this.Init();
        }

        private void Init()
        {
            this.IdTipoEvento = 0;
            this.Descripcion = string.Empty;
        }

        public bool Read()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                Datos.TipoEvento evento = bbdd.TipoEvento.First(tp => tp.IdTipoEvento == IdTipoEvento);
                CommonBC.Syncronize(evento, this);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<TipoEvento> ReadAll()
        {
            Datos.OnBreakEntities bbdd = new Datos.OnBreakEntities();

            try
            {
                List<Datos.TipoEvento> listadoDatos = bbdd.TipoEvento.ToList<Datos.TipoEvento>();

                List<TipoEvento> listadoNegocio = GenerarListado(listadoDatos);

                return listadoNegocio;
            }
            catch (Exception ex)
            {
                return new List<TipoEvento>();
            }
        }

        private List<TipoEvento> GenerarListado(List<Datos.TipoEvento> listadoTipoEvento)
        {
            List<TipoEvento> listadoEvento = new List<TipoEvento>();

            foreach(Datos.TipoEvento dato in listadoTipoEvento)
            {
                TipoEvento evento = new TipoEvento();
                CommonBC.Syncronize(dato, evento);

                listadoEvento.Add(evento);
            }
            return listadoEvento;
        }
    }
}
