﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;

namespace OnBreak.WPF
{
    /// <summary>
    /// Lógica de interacción para VentanaPrincipal.xaml
    /// </summary>
    public partial class VentanaPrincipal : MetroWindow
    {
        public VentanaPrincipal()
        {
            InitializeComponent();
        }

        private void BtnAdministracionClientes_Click(object sender, RoutedEventArgs e)
        {
            VentanaRegistro clientes = new VentanaRegistro();
            clientes.Owner = this;
            clientes.ShowDialog();
        }

        private void BtnAdministracionContratos_Click(object sender, RoutedEventArgs e)
        {
            VentanaAdministracionContratos contratos = new VentanaAdministracionContratos();
            contratos.Owner = this;
            contratos.ShowDialog();
        }

        private void BtnListadoClientes_Click(object sender, RoutedEventArgs e)
        {
            VentanaConsulta listadoClientes = new VentanaConsulta();
            listadoClientes.Owner = this;
            listadoClientes.ShowDialog();
        }

        private void BtnListadoContratos_Click(object sender, RoutedEventArgs e)
        {
            VentanaListadoContratos listadoContratos = new VentanaListadoContratos();
            listadoContratos.Owner = this;
            listadoContratos.ShowDialog();
        }
    }
}
