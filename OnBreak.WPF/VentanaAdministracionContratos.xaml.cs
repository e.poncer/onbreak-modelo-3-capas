﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using OnBreak.Negocio;

namespace OnBreak.WPF
{
    /// <summary>
    /// Lógica de interacción para VentanaAdministracionContratos.xaml
    /// </summary>
    public partial class VentanaAdministracionContratos : MetroWindow
    {
        public VentanaAdministracionContratos()
        {
            InitializeComponent();

            CargarModalidad();

            CargarTipoEvento();
        }

        private void CargarModalidad()
        {
            cbModalidad.ItemsSource = new ModalidadServicio().ReadAll();
            cbModalidad.DisplayMemberPath = "Nombre";
            cbModalidad.SelectedValuePath = "IdModalidad";
            cbModalidad.SelectedIndex = -1;
        }

        private void CargarTipoEvento()
        {
            cbTipoEvento.ItemsSource = new TipoEvento().ReadAll();
            cbTipoEvento.DisplayMemberPath = "Descripcion";
            cbTipoEvento.SelectedValuePath = "IdTipoEvento";
            cbTipoEvento.SelectedIndex = -1;
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Contrato cont = new Contrato()
                {
                    Numero = txtNumeroContrato.Text,
                    Creacion = (DateTime)dpCreacion.SelectedDate,
                    Termino = (DateTime)dpFechaTermino.SelectedDate,
                    RutCliente = txtRutCliente.Text,
                    IdModalidad = (string)cbModalidad.SelectedValue,
                    IdTipoEvento = (int)cbTipoEvento.SelectedValue,
                    FechaHoraInicio = (DateTime)dpFechaHoraInicio.SelectedDate,
                    FechaHoraTermino = (DateTime)dpFechaHoraFinal.SelectedDate,
                    Asistentes = int.Parse(txtCantidadAsistentes.Text),
                    PersonalAdicional = int.Parse(txtCantidadPersonal.Text),
                    Realizado = (bool)chkRealizado.IsChecked,
                    ValorTotalContrato = double.Parse(txtValorFinal.Text),
                    Observaciones = txtObservaciones.Text
                };

                if (!cont.Read())
                {
                    if (cont.Create())
                    {
                        MessageBox.Show("El contrato fue registrado exitosamente", "Informacion", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("El contrato de cliente no registrada", "Atencion", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("El contrato del cliente ya existe", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El contrato no pudo ser creado , porque faltan algunos datos");
            }
        }

        private async void BtnTerminarContratos_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var x = await this.ShowProgressAsync("Aplicando Termino Del Contrato", "Espere 5 segundos....");
                await Task.Delay(5000);

                int valorBase, cantAsistentes, cantPersonalAdicional;

                txtEstado.Text = "No vigente";
                dpCreacion.SelectedDate = DateTime.Today;

                if (txtValorBase.Text == string.Empty)
                {
                    MessageBox.Show("El campo de valor base no puede estar vacio");
                    return;
                }else if (!int.TryParse(txtValorBase.Text, out valorBase))
                {
                    MessageBox.Show("El valor base debe ser un numero");
                    return;
                }

                if (txtCantidadAsistentes.Text == string.Empty)
                {
                    MessageBox.Show("El campo de cantidad de asistentes no puede estar vacio");
                    return;
                }
                else if (!int.TryParse(txtCantidadAsistentes.Text, out cantAsistentes))
                {
                    MessageBox.Show("La cantidad de asistentes debe ser un numero");
                    return;
                }

                if (txtCantidadPersonal.Text == string.Empty)
                {
                    MessageBox.Show("El campo de cantidad del personal no puede estar vacio");
                    return;
                }
                else if (!int.TryParse(txtCantidadPersonal.Text, out cantPersonalAdicional))
                {
                    MessageBox.Show("La cantidad personal debe ser un numero");
                    return;
                }

                double total = new Valorizador().CalcularValorEvento(valorBase, cantAsistentes, cantPersonalAdicional);

                txtValorFinal.Text = total.ToString();

                await x.CloseAsync().ConfigureAwait(false);

             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            var salir = new MetroDialogSettings()
            {
                AffirmativeButtonText = "Si",
                NegativeButtonText = "No",
            };

            MessageDialogResult result = await this.ShowMessageAsync("Confirmacion", "¿Esta Seguro De Cancelar El Contrato?",
                MessageDialogStyle.AffirmativeAndNegative, salir);

            if (result == MessageDialogResult.Affirmative)
            {
                this.Close();
            }
        }

        private void BtnActualizar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Contrato cont = new Contrato()
                {
                    Numero = txtNumeroContrato.Text,
                    Creacion = (DateTime)dpCreacion.SelectedDate,
                    Termino = (DateTime)dpFechaTermino.SelectedDate,
                    RutCliente = txtRutCliente.Text,
                    IdModalidad = (string)cbModalidad.SelectedValue,
                    IdTipoEvento = (int)cbTipoEvento.SelectedValue,
                    FechaHoraInicio = (DateTime)dpFechaHoraInicio.SelectedDate,
                    FechaHoraTermino = (DateTime)dpFechaHoraFinal.SelectedDate,
                    Asistentes = int.Parse(txtCantidadAsistentes.Text),
                    PersonalAdicional = int.Parse(txtCantidadPersonal.Text),
                    Realizado = (bool)chkRealizado.IsChecked,
                    ValorTotalContrato = double.Parse(txtValorFinal.Text),
                    Observaciones = txtObservaciones.Text
                };

                if (cont.Update())
                {
                    MessageBox.Show("El contrato del cliente modificado", "Informacion", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("El contrato del cliente no pudo ser modificada", "Atencion", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El contrato no se pudo actualizar, ya que faltan algunos datos");
            }
        }

        private void BtnBuscarContrato_Click(object sender, RoutedEventArgs e)
        {
            VentanaListadoContratos win = new VentanaListadoContratos();
            win.ShowDialog();

            if (win.ContratoSeleccionado != null)
            {
                LlenarContrato(win.ContratoSeleccionado);
            }
        }

        private void LlenarContrato(Contrato cont)
        {
            try
            {
                txtNumeroContrato.Text = cont.Numero;
                txtObservaciones.Text = cont.Observaciones;
                dpCreacion.SelectedDate = cont.Creacion;
                dpFechaTermino.SelectedDate = cont.Termino;
                dpFechaHoraInicio.SelectedDate = cont.FechaHoraInicio;
                dpFechaHoraFinal.SelectedDate = cont.FechaHoraTermino;
                chkRealizado.IsChecked = cont.Realizado;
                txtRutCliente.Text = cont.RutCliente;
                cbModalidad.SelectedValue = cont.IdModalidad;
                cbTipoEvento.SelectedValue = cont.IdTipoEvento;
                txtCantidadAsistentes.Text = cont.Asistentes.ToString();
                txtCantidadPersonal.Text = cont.PersonalAdicional.ToString();
                txtValorFinal.Text = cont.ValorTotalContrato.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnBuscarNumero_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Contrato cont = new Contrato()
                {
                    Numero = txtNumeroContrato.Text,
                };

                if (cont.Read())
                {
                    txtObservaciones.Text = cont.Observaciones;
                    dpCreacion.SelectedDate = cont.Creacion;
                    dpFechaTermino.SelectedDate = cont.Termino;
                    dpFechaHoraInicio.SelectedDate = cont.FechaHoraInicio;
                    dpFechaHoraFinal.SelectedDate = cont.FechaHoraTermino;
                    chkRealizado.IsChecked = cont.Realizado;
                    txtRutCliente.Text = cont.RutCliente;
                    cbModalidad.SelectedValue = cont.IdModalidad;
                    cbTipoEvento.SelectedValue = cont.IdTipoEvento;
                    txtCantidadAsistentes.Text = cont.Asistentes.ToString();
                    txtCantidadPersonal.Text = cont.PersonalAdicional.ToString();
                    txtValorFinal.Text = cont.ValorTotalContrato.ToString();
                }
                else
                {
                    MessageBox.Show("El numero de contrato no existe", "Atencion", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnBuscarRut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cliente cli = new Cliente()
                {
                    RutCliente = txtRutCliente.Text,
                };

                if (cli.Read())
                {
                    txtNombre.Text = cli.NombreContacto;
                }
                else
                {
                    MessageBox.Show("El rut del cliente no existe", "Atencion", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CbModalidad_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbModalidad.SelectedIndex == 0)
                {
                    txtValorBase.Text = "3";
                    txtCantidadPersonal.Text = "2";
                }

                if (cbModalidad.SelectedIndex == 1)
                {
                    txtValorBase.Text = "8";
                    txtCantidadPersonal.Text = "6";
                }

                if (cbModalidad.SelectedIndex == 2)
                {
                    txtValorBase.Text = "12";
                    txtCantidadPersonal.Text = "6";
                }

                if (cbModalidad.SelectedIndex == 3)
                {
                    txtValorBase.Text = "25";
                    txtCantidadPersonal.Text = "10";
                }

                if (cbModalidad.SelectedIndex == 4)
                {
                    txtValorBase.Text = "35";
                    txtCantidadPersonal.Text = "14";
                }

                if (cbModalidad.SelectedIndex == 5)
                {
                    txtValorBase.Text = "6";
                    txtCantidadPersonal.Text = "4";
                }

                if (cbModalidad.SelectedIndex == 6)
                {
                    txtValorBase.Text = "10";
                    txtCantidadPersonal.Text = "5";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
    }
}
