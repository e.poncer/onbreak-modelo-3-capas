﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using OnBreak.Negocio;

namespace OnBreak.WPF
{
    /// <summary>
    /// Lógica de interacción para VentanaRegistro.xaml
    /// </summary>
    public partial class VentanaRegistro : MetroWindow
    {
        public VentanaRegistro()
        {
            InitializeComponent();

            LimpiarControles();
        }

        private void LimpiarControles()
        {
            txtRut.Text = string.Empty;
            txtRazonSocial.Text = string.Empty;
            txtNombreContacto.Text = string.Empty;
            txtMailContacto.Text = string.Empty;
            txtDireccion.Text = string.Empty;
            txtTelefono.Text = string.Empty;

            CargarTiposDeActividad();

            CargarTiposDeEmpresa();

            CargarClientes();
        }

        private void CargarClientes()
        {
            dgrClientes.ItemsSource = new Cliente().ReadAll();
        }

        private void CargarTiposDeEmpresa()
        {
            cbTipo.ItemsSource = new TipoEmpresa().ReadAll();
            cbTipo.DisplayMemberPath = "Descripcion";
            cbTipo.SelectedValuePath = "IdTipoEmpresa";
            cbTipo.SelectedIndex = -1;
        }

        private void CargarTiposDeActividad()
        {
            cbActividad.ItemsSource = new ActividadEmpresa().ReadAll();
            cbActividad.DisplayMemberPath = "Descripcion";
            cbActividad.SelectedValuePath = "IdActividadEmpresa";
            cbActividad.SelectedIndex = -1;
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cliente cli = new Cliente()
                {
                    RutCliente = txtRut.Text,
                    RazonSocial = txtRazonSocial.Text,
                    NombreContacto = txtNombreContacto.Text,
                    MailContacto = txtMailContacto.Text,
                    Direccion = txtDireccion.Text,
                    Telefono = txtTelefono.Text,
                    IdActividadEmpresa = (int)cbActividad.SelectedValue,
                    IdTipoEmpresa = (int)cbTipo.SelectedValue
                };

                if (!cli.Read())
                {
                    if (cli.Create())
                    {
                        MessageBox.Show("Registro de cliente registrada", "Informacion", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarControles();
                    }
                    else
                    {
                        MessageBox.Show("Registro de cliente no registrada", "Atencion", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Registro de cliente ya existe", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El registro de cliente no fue creado correctamente , ya que los datos estan vacios");
            }
        }

        private void BtnActualizar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cliente cli = new Cliente()
                {
                    RutCliente = txtRut.Text,
                    RazonSocial = txtRazonSocial.Text,
                    NombreContacto = txtNombreContacto.Text,
                    MailContacto = txtMailContacto.Text,
                    Direccion = txtDireccion.Text,
                    Telefono = txtTelefono.Text,
                    IdActividadEmpresa = (int)cbActividad.SelectedValue,
                    IdTipoEmpresa = (int)cbTipo.SelectedValue
                };

                if (cli.Update())
                {
                    MessageBox.Show("Registro de cliente modificado", "Informacion", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarControles();
                }
                else
                {
                    MessageBox.Show("Registro de cliente no puede ser modificado", "Atencion", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El registro de cliente no se pudo actualizar , ya que faltan algunos datos");
            }
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cliente cli = new Cliente()
                {
                    RutCliente = txtRut.Text,
                };

                MessageBoxResult eliminar = MessageBox.Show("¿Seguro que quiere eliminar este registro de cliente?", "Confirmar", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (eliminar == MessageBoxResult.Yes)
                {
                    if (cli.Delete())
                    {
                        MessageBox.Show("Registro de cliente eliminado", "Informacion", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarControles();
                    }
                    else
                    {
                        MessageBox.Show("Registro de cliente no puede ser eliminado", "Atencion", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cliente cli = new Cliente()
                {
                    RutCliente = txtRut.Text,
                };

                if (cli.Read())
                {
                    txtRut.Text = cli.RutCliente;
                    txtRazonSocial.Text = cli.RazonSocial;
                    txtNombreContacto.Text = cli.NombreContacto;
                    txtMailContacto.Text = cli.MailContacto;
                    txtDireccion.Text = cli.Direccion;
                    txtTelefono.Text = cli.Telefono;
                    cbActividad.SelectedValue = cli.IdActividadEmpresa;
                    cbTipo.SelectedValue = cli.IdTipoEmpresa;


                    MessageBox.Show("El registro de cliente fue encontrado exitosamente", "Informacion", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    MessageBox.Show("Registro de cliente no pudo ser leida o no existe", "Atencion", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnBuscarCliente_Click(object sender, RoutedEventArgs e)
        {
            VentanaConsulta win = new VentanaConsulta();
            win.ShowDialog();

            if (win.ClienteSeleccionado != null)
            {
                LlenarCliente(win.ClienteSeleccionado);
                dgrClientes.ItemsSource = new Cliente().ReadAll();
                dgrClientes.Items.Refresh();
            }
        }

        private void LlenarCliente(Cliente cli)
        {
            txtRut.Text = cli.RutCliente;
            txtRazonSocial.Text = cli.RazonSocial;
            txtNombreContacto.Text = cli.NombreContacto;
            txtMailContacto.Text = cli.MailContacto;
            txtDireccion.Text = cli.Direccion;
            txtTelefono.Text = cli.Telefono;
            cbActividad.SelectedValue = cli.IdActividadEmpresa;
            cbTipo.SelectedValue = cli.IdTipoEmpresa;
        }

        private async void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            var salir = new MetroDialogSettings()
            {
                AffirmativeButtonText = "Si",
                NegativeButtonText = "No",
            };

            MessageDialogResult result = await this.ShowMessageAsync("Confirmacion", "¿Seguro de cerrar la aplicacion?",
                MessageDialogStyle.AffirmativeAndNegative, salir);

            if (result == MessageDialogResult.Affirmative)
            {
                this.Close();
            }
        }
    }
}
