﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using OnBreak.Negocio;

namespace OnBreak.WPF
{
    /// <summary>
    /// Lógica de interacción para VentanaListadoContratos.xaml
    /// </summary>
    public partial class VentanaListadoContratos : MetroWindow
    {
        public Contrato ContratoSeleccionado { get; set; }

        public VentanaListadoContratos()
        {
            InitializeComponent();

            CargarModalidad();

            CargarContratos();
        }

        private void CargarModalidad()
        {
            cbModalidad.ItemsSource = new ModalidadServicio().ReadAll();
            cbModalidad.DisplayMemberPath = "Nombre";
            cbModalidad.SelectedValuePath = "IdModalidad";
            cbModalidad.SelectedIndex = -1;
        }

        private void CargarContratos()
        {
            dgrContratos.ItemsSource = new Contrato().ReadAll();
        }

        private void BtnBuscarRut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string rut = txtRut.Text;
                dgrContratos.ItemsSource = new Contrato().ReadAllByRut(rut);

                if (rut == string.Empty)
                {
                    MessageBox.Show("Para iniciar la busqueda por rut , digite un rut correspondiente");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnBuscarNumero_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string numeroContrato = txtNumeroContrato.Text;
                dgrContratos.ItemsSource = new Contrato().ReadAllByNumeroContrato(numeroContrato);

                if (numeroContrato == string.Empty)
                {
                    MessageBox.Show("Para iniciar la busqueda por numero de contrato , debe digitar un numero de contrato");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnModalidad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string modalidad = (string)cbModalidad.SelectedValue;
                dgrContratos.ItemsSource = new Contrato().ReadAllByTipo(modalidad);

                if (cbModalidad.SelectedIndex == -1)
                {
                    MessageBox.Show("Debe seleccionar una modalidad de la lista , para iniciar la busqueda");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnAdministrarContratos_Click(object sender, RoutedEventArgs e)
        {
            VentanaAdministracionContratos win = new VentanaAdministracionContratos();
            win.ShowDialog();
        }

        private void BtnEditarClientes_Click(object sender, RoutedEventArgs e)
        {
            if (dgrContratos.SelectedValue == null)
            {
                MessageBox.Show("Debe seleccionar un contrato");
                return;
            }
            ContratoSeleccionado = (Contrato)dgrContratos.SelectedValue;

            this.Close();
        }
    }
}
