﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using OnBreak.Negocio;

namespace OnBreak.WPF
{
    /// <summary>
    /// Lógica de interacción para VentanaConsulta.xaml
    /// </summary>
    public partial class VentanaConsulta : MetroWindow
    {
        public Cliente ClienteSeleccionado { get; set; }

        public VentanaConsulta()
        {
            InitializeComponent();

            CargarActividadEmpresa();

            CargarClientes();
        }

        private void CargarClientes()
        {
            dgrClientes.ItemsSource = new Cliente().ReadAll();
        }

        private void CargarActividadEmpresa()
        {
            cbActividad.ItemsSource = new ActividadEmpresa().ReadAll();
            cbActividad.DisplayMemberPath = "Descripcion";
            cbActividad.SelectedValuePath = "IdActividadEmpresa";
            cbActividad.SelectedIndex = -1;
        }

        private void BtnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            txtRut.Text = string.Empty;
            txtTipo.Text = string.Empty;
            cbActividad.SelectedIndex = -1;
        }

        private void BtnBuscarRut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string rut = txtRut.Text;
                dgrClientes.ItemsSource = new Cliente().ReadAllByRut(rut);

                if (rut == string.Empty)
                {
                    MessageBox.Show("El rut no puede estar vacio , digite un rut para iniciar la busqueda");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnTipoEmpresa_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string tipo = txtTipo.Text;
                dgrClientes.ItemsSource = new Cliente().ReadAllByTipoEmpresa(tipo);

                if (tipo == string.Empty)
                {
                    MessageBox.Show("El tipo de empresa no puede estar vacio, digite para iniciar la busqueda");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnActividadEmpresa_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int actividad = (int)cbActividad.SelectedValue;
                dgrClientes.ItemsSource = new Cliente().ReadAllByActividad(actividad);

                if (cbActividad.SelectedIndex == -1)
                {
                    MessageBox.Show("El campo no puede estar vacio , elija uno de la lista");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dgrClientes.SelectedValue == null)
            {
                MessageBox.Show("Debe seleccionar a un cliente");
                return;
            }
            ClienteSeleccionado = (Cliente)dgrClientes.SelectedValue;

            this.Close();
        }

        private void BtnAdministrarContratos_Click(object sender, RoutedEventArgs e)
        {
            VentanaAdministracionContratos win = new VentanaAdministracionContratos();
            win.ShowDialog();
        }

        private void BtnListarContratos_Click(object sender, RoutedEventArgs e)
        {
            VentanaListadoContratos win = new VentanaListadoContratos();
            win.ShowDialog();
        }
    }
}
