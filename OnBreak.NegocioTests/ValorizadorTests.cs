﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnBreak.Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Negocio.Tests
{
    [TestClass()]
    public class ValorizadorTests
    {
        [TestMethod()]
        public void CalcularValorEventoTest()
        {
            OnBreak.Negocio.Valorizador calculo = new Valorizador();

            try
            {
                calculo.CalcularValorEvento(500, 10, 3);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Onbreak");
                return;
            }

            Assert.Fail("No se pudo calcular el evento");

        }
    }
}